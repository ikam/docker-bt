# docker-宝塔
> docker下运行宝塔面板，实现本地完全隔离的开发环境！该脚本可以实现一键安装！
> 安装完成后，登录宝塔面板后台进行所需环境安装即可直接使用啦！
> 宝塔面板升级等一些列操作和Linux虚拟机上面安装一模一样
> 该安装脚本在[ifui/baota](https://github.com/ifui/baota) 基础上进行了一点点的改进，更符合自己的使用习惯～
> 使用技巧，挂载本机的目录都可以创建软连接，这样你就可以把你之前放项目的目录软链到当前项目下面docker挂载的wwwroot目录下即可！
> 软链命令`ln -sfv /Users/ieras/Sites /Users/ieras/bt/wwwroot`
> 软链后记得停止下镜像`docker-compose stop app`,然后再启动镜像` docker-compose up -d app && docker exec -it app /bin/zsh`即可生效

- 镜像默认使用 `Centos7` 基础镜像，使用 `Dockerfile` 构建
- 该脚本每次 `build` 都会保留之前信息，当然如果你删除了卷那没办法。。。配置文件里可以配置默认用户名和密码

- 安装时间视网络环境而定，建议更换 `Docker` 的国内源

- 配置文件 `.env` 中都有详细的注释

- 使用 `volume` 作持久化存储，数据卷名为 `bt_www` `bt_usr` `bt_etc`

## 更新记录

- 2021-03-12 废弃dns、废弃数据库存储磁盘(严重影响性能)、默认安装了zsh，配合iTerm进容器更漂亮，安装过程看install.jpg

## 如何使用

> 建议使用 Liunx 或者 MAC 部署，windows用户想来是用不到这个

### 1. 安装git，或者直接下载`zip`也可以
`sudo yum install -y git`

### 2. 到你想生成项目的文件夹下执行命令
`git clone https://gitee.com/ikam/docker-bt.git ./bt`

### 3. 进入项目根目录
`cd bt`

### 4. 生成配置文件
`cp .env-example .env`

### 5. 启动宝塔镜像，在项目根目录下执行命令
`docker-compose up -d app`
或者 启动并进入ssh(日常开发，往往需要ssh操作)
`docker-compose up -d app && docker exec -it app /bin/zsh`

### 6. 查看默认登录信息
`docker-compose logs app`
也可以不用看，因为配置文件默认配置好了用户名和密码

## 如何进行数据备份和迁移

### 1. 首先正常部署成功后，将需要的应用程序和配置安装和设置完毕

### 2. 启动并进入`app_backup`容器，注意：接下来的操作都是在该容器下的交互命令下执行
```bash
docekr-compose stop
docekr-compose up -d app_backup
docekr-compose exec app_backup sh
```

#### 3.1 备份

> 执行成功后会在宿主机项目目录下的`app_backup/export`目录下生成`baota_backup_*.tar.gz`的数据包

`sh /app_backup/export.sh`

#### 3.2 迁移

> 将数据包放在`app_backup/export`目录下，然后执行，根据提示操作即可

`sh /app_backup/import.sh`

## 其他说明

### 目录结构

- app
  - app.sh 宝塔镜像启动脚本
  - Dockerfile
- app_backup
  - app_backup 宝塔数据备份迁移脚本
  - Dockerfile
  - export.sh 导出脚本
  - import.sh 导入脚本
- backup .env可配置，默认为宝塔备份目录
- wwwlogs .env可配置，默认为宝塔日志目录
- wwwroot .env可配置，默认为宝塔网站目录，请把你的网站放在此目录下

### .env配置说明
> 这里可以自定义端口和目录，请酌情设置，默认也可
> 没加入cron路径，因为测试发现用不了
> 另外没有实现运行到/usr/sbin/init下面，所以计划任务不能自动执行
```
# 宝塔安装地址URL
BT_INSTALL_PATH=http://download.bt.cn/install/install_6.0.sh

# 面板账户
BT_USERNAME=baota
BT_PASSWORD=123456
# DNS(废弃，配置保留不管即可)
DNS1=8.8.8.8 
DNS2=114.114.114.114

# Driver
VOLUMES_DRIVER=local
# bridge / host
NETWORKS_DRIVER=bridge

# Centos 版本
CENTOS_VERSION=7

# PORT 开放端口
# 面板端口
BT_PORT=8887
# 网站默认端口
WEB_PORT=80
# HTTPS 端口
HTTPS_PORT=443
# FTP 端口
FTP_PORT=21
# FTP 数据传输端口
FTP_DATA_PORT=20
# SSH 端口
SSH_PORT=22000
# MYSQL 端口
MYSQL_PORT=3306
# PhpMyAdmin 端口
PHPMYADMIN_PORT=888
# redis 端口
REDIS_PORT=6379
# memcached 端口
MEMCACHED_PORT=11211

#XDEBUG
XDEBUG_PORT = 9003

# PATH 路径
# 网站默认路径
WEB_PATH=./wwwroot
# 日志
LOGS_PATH=./wwwlogs
# 数据库相关存储路径(废弃，严重影响性能，大家自己做好数据库备份吧)
DATA_PATH=./data
# 宝塔备份
BACKUP_PATH=./backup
```

### 常用命令

```bash
# 构建容器
docker-compose build
# 不缓存构建，执行后默认登录信息会变化
docker-compose build --no-cache
# 查看运行情况
docker-compose ps
# 启动宝塔镜像
docker-compose up -d app
# 启动宝塔镜像并进入命令行
docker-compose up -d app && docker exec -it app /bin/zsh
# 启动宝塔数据备份迁移系统
docker-compose up -d app_backup
# 启动所有
docker-compose up -d
# 停止运行
docker-compose stop app
# 删除容器和数据卷
docker-compose down --volumes
# 列出全部的数据卷
docker volume ls
# 删除指定的多个数据卷
docker volume rm volume_name volume_name
```

## 鸣谢
大家有好的修改建议或者问题，欢迎留言，我会及时回复问题和采纳建议！另外也期望能有高手能搞出完美的/usr/sbin/init模式下运行方案
