#!/bin/bash

BTUSERNAME=baota
BTPASSWORD=123456
DNS1=8.8.8.8
DNS2=8.8.4.4

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

Main()
{
    Init
    /etc/init.d/bt default
}

Init()
{
    # echo nameserver $DNS1 >/etc/resolv.conf
    # echo nameserver $DNS2 >>/etc/resolv.conf
    # echo options ndots:0 >>/etc/resolv.conf

    if [ ! -f "/etc/init.d/bt_install.lock" ]; then
        # 清除登陆限制
        rm -f /www/server/panel/data/admin_path.pl
        # 修改面板username
        sed -i 's/set_panel_username()/set_panel_username(sys.argv[2])/' /www/server/panel/tools.py
        cd /www/server/panel && python tools.py username $BTUSERNAME
        sed -i 's|set_panel_username(sys.argv\[2\])|set_panel_username()|' /www/server/panel/tools.py
        # 修改面板密码
        cd /www/server/panel && python tools.py panel $BTPASSWORD
        touch "/etc/init.d/bt_install.lock"
    fi
    # 启动服务
    cd /etc/init.d
    for i in `ls`
    do
    if [ -x $i ]; then
        ./$i start
    fi
    done
}

Main
/bin/bash